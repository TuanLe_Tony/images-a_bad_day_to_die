
# A BAD DAY TO DIE

Sample code for a time play app. Written in Android studio / Java ,Firebase , Google map.

## Software Requirements - Scenario 
###  VIrtual Reality - Real world actertion (GAME)

 
 
# After signed in -> Player's Details - Screen

![Scheme](images/userdetails.png)

### Click player UI -> can upload your image.
### SCORE: Real time update from firebase.
### SCORE reachs 100 -> Rank Image will change to new image.
### -> START CHALLENGE  (button) -> display new screen below.

# Map - Screen
![Scheme](images/map.png)

### - If A trap is nearby player's location -> Show the box
### - If The Box was clicked -> randomly display a new screen game (Shaking game, Math Game, Avoid Falling Plane Reality)

# Shaking Game - Screen
## Shaking your phone will randomly pick a image.
![Scheme](images/shaking1.png)
![Scheme](images/shaking2.png)

# Math game - Screen
## Player needs to calculate the answer - in count down 30s
### - the game is not static -> hard to predict the answer.
![Scheme](images/mathgame.png)
### UI - Input answer and click button to confirm answer.

# Avoid Falling Plane Reality - Screen
## Showing player's current location with player's image - plane image.
![Scheme](images/planeisfalling.png)
### - Player has 240 seconds (4 minutes) to run out of dangerous zone.
### - UI - Click plane image on screen -> calculated and show distance between current location and plane. (Set distance information to text view on screen)
## IF YOU LOST -> Display a death screen below

# Death - Screen
## Showing Answer selected for question 2
![Scheme](images/deathscreen.png)
### - Player able to watch videos
### - end of video return to player's details screen.

# Map Challenge Player -  Screen
## UI - Click to marker -> getting name, lat and long of player -> then check if player's is nearby your current location -> go to waiting room game.
![Scheme](images/challengeplayer.png)

# Waiting Room - Screen
## Show Player's name
![Scheme](images/waitingroom.png)
![Scheme](images/waitingroomstarted.png)
### Button click -> bring 2 players to the game

# Space Ship Game - Screen
## 2 Players Interaction.
![Scheme](images/spaceship1.png)
### - 2 player getting the fight in the game.











